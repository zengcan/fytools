<?php

class ComFun
{
	
	
	static function json_encode($input){
		// 从 PHP 5.4.0 起, 增加了这个选项.
		if(defined('JSON_UNESCAPED_UNICODE')){
			return json_encode($input, JSON_UNESCAPED_UNICODE);
		}
		if(is_string($input)){
			$text = $input;
			$text = str_replace('\\', '\\\\', $text);
			$text = str_replace(
					array("\r", "\n", "\t", "\""),
					array('\r', '\n', '\t', '\\"'),
					$text);
			return '"' . $text . '"';
		}else if(is_array($input) || is_object($input)){
			$arr = array();
			$is_obj = is_object($input) || (array_keys($input) !== range(0, count($input) - 1));
			foreach($input as $k=>$v){
				if($is_obj){
					$arr[] = self::json_encode($k) . ':' . self::json_encode($v);
				}else{
					$arr[] = self::json_encode($v);
				}
			}
			if($is_obj){
				return '{' . join(',', $arr) . '}';
			}else{
				return '[' . join(',', $arr) . ']';
			}
		}else{
			return $input . '';
		}
	}
	
	static function get_result_from_url2($url)
	{
		$content = file_get_contents($url);
		
		if(!$content)
		{
			echo "content error";
		}
		else
		{
			//echo $content;
		}
		//$content=iconv('gbk','utf-8', $content);
		$result = json_decode($content);
		
		if($result)
		{
			if($result->error_code==0)
			{
				return $result;
			}else
			{
				echo $result['error_code'].":".$result['reason'];
			}
		}
		else
		{
			echo "json_decode error";
		}
		
		return null;
	}

	static function get_result_from_url($url)
	{
		
		//json_decode只支持utf8,一定要转换
		//$content=iconv('gbk','utf-8', $content);
		
		$content = file_get_contents($url);
	
		if(!$content)
		{
			echo "content error";
			
			return null;
		}
		else 
		{
		//	echo $content;
		}
		//$content=iconv('gbk','utf-8', $content);
			$result = json_decode($content,true);
			
			
		if($result){
			if($result['error_code']=='0')
			{				
				return $result['result'];						
			}else
			{
				
				//echo $result['error_code'].":".$result['reason'];
				return null;
			}
		}
		else
		{
		//	echo "json_decode error";
			return null;
		}
		
		return  null;
	}
	

static function gbkToUTF8($str)
{
	$str=iconv('gbk','utf-8', $str);
	
	return $str;
}


}