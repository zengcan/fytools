/**
 * Created by asus2016 on 2016/7/16.
 */
var Illegal={
    province:null,//省
    provinceList:[],
    city:null,//城市
    cityList:[],//城市列表
    carNum:null,//车牌号
    hasEngine:false,
    engine:null,//车辆发动机号码
    hasFrameNum:false,
    frameNum:null,//车辆车架号码
    AllList:null,
    init:function(){
        var __this = this;
        __this.makeProvinceSelect();
        __this.changeCity("BJ");
        __this.changeOther();
    },
    getCity:function(){
        var __this = this;
        $.ajax({
            type: "POST",
            contentType: "",
            dataType: "json",
            url: "plugin.php?id=fytools:illegal&query=city",//传入后台的地址/方法
            data: "",//参数，这里是一个json语句
            success: function (data) {
                __this.AllList = data;
                console.log(__this.AllList);
                __this.getProvince();
                __this.init();
            },
            error: function (err) {
                alert("err:" + err);
                window.location.reload();
            }
        });
    },
    returnCityList:function(){
    },
    getProvince:function(){
        var __this = this;
        for(var i=0;i<__this.AllList.length;i++){
            var obj = {
                "p_name":"",
                "p_code":""
            };
            obj.p_name = __this.AllList[i].province;
            obj.p_code = __this.AllList[i].province_code;
            __this.provinceList.push(obj);
        }
        console.log(__this.provinceList);
    },
    makeProvinceSelect:function(){
        var __this = this;
        var provinceList = __this.provinceList;
        var optionList = "";
        var obj = "";
        for(var i=0;i<provinceList.length;i++){
            obj = "<option value ='"+provinceList[i].p_code+"'>"+provinceList[i].p_name+"</option>"
            optionList = optionList + obj;
        }
        $("#province").html(optionList);
    },
    changeCity:function(pcode){
        var __this = this;
        var p_code = pcode;
        var obj = "";
        var cityList = "";
        for(var i=0;i<__this.AllList.length;i++){
            if(__this.AllList[i].province_code == p_code){
                __this.cityList = __this.AllList[i].citys;
            }
        }
        for(var i = 0;i<__this.cityList.length;i++){
            var tcity = __this.cityList[i];
            obj = "<option value ='"+tcity.city_code
                +"'data-engine='"+tcity.engine+"'data-engines='"+tcity.engineno
                +"'data-class='"+tcity.class+"'datya-classno='"+tcity.classno
                +"'>"+tcity.city_name+"</option>"
            cityList = cityList + obj;
        }
        $("#city").html(cityList);

    },
    changeOther:function(){
        var __this = this;
        var toption = $("#city option:selected");
        if(toption.attr("data-engine")=="0"){
            $("#engine-list").addClass("display-none");
            __this.hasEngine = false;
        }else{
            __this.hasEngine = true;
            $("#engine-list").removeClass("display-none");
            if(toption.attr("data-engines")=="0"){
                $("#engine-list .error").html("请输入完整发动机号")

            }
            else{
                $("#engine-list .error").html("请输入发动机后"+toption.attr("data-engines")+"位")
            }
        }

        if(toption.attr("data-class")=="0"){
            __this.hasFrameNum = false;
            $("#frame-num-list").addClass("display-none");
        }else{
            __this.hasFrameNum = true;
            $("#frame-num-list").removeClass("display-none");
            if(toption.attr("datya-classno")=="0"){
                $("#frame-num-list .error").html("请输入完整车架号")
            }
            else{
                $("#frame-num-list .error").html("请输车架号后"+toption.attr("datya-classno")+"位")
            }
        }


    },
    checkCarNum:function(){
        if($("#car-num").val().trim()=="")
            return false;
        else
            return true;
    },
    checkEngine:function(){
        if($("#engine").val().trim()=="")
            return false;
        else return true;
    },
    checkFrameNum:function(){
        if($("#frame-num").val().trim()=="")
            return false;
        else return true;
    },
    submit:function(){
        var __this = this;
        var tips = "请按要求填写："
        var result = true;
        if(!__this.checkCarNum()){
            $("#AllTip").removeClass("display-none")
            tips = tips + "车牌号、"
            result = false;
        }
        if(__this.hasEngine){
            if(!__this.checkEngine())
            {
                tips = tips + "发动机号、"
                result = false;
            }
        }
        if(__this.hasFrameNum){
            if(!__this.checkFrameNum())
            {
                tips = tips + "车架号、"
                result = false;
            }
        }
        if(!result){
            console.log(tips)
            $("#AllTip").html(tips);
            $("#AllTip").removeClass("display-none")
        }
        return result;
    }
}

$(document).ready(function(){
    var __this = Illegal;
    __this.getCity();

    
    $("#province").change(function(){
        var options = $("#province option:selected");
         __this.changeCity(options.val());
        __this.changeOther();
    })
    
    $("#city").change(function(){
        __this.changeOther();
     })

    $("#check").click(function(){
        if(__this.submit()){
            $("#Illegal-form").submit();
        }
    })
    $("input[type='text']").click(function(){
        $("#AllTip").addClass("display-none");
    })
})

